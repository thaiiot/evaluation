# TinyGS

TinyGS is the open source global satellite network which have the ground stations distributed around the world to receive and operate LoRa satellites, weather probes and other flying objects, using cheap and versatile modules.
It is currently compatible with sx126x and sx127x LoRa modules.

For more information about TinyGS, see https://tinygs.com

![](TinyGS/images/TinyGS_Website.png)


### How THAIIOT test wih TinyGS network
To prove that THAIIOT CubeSat is able to communicate to the LEO (Low-Earth-Orbit) distance.
THAIIOT Engineering Model (EM) will operate as the ground station in the TinyGS Ground Station Network, but only listening.


### Nordy, LoRa 6U CubeSat
**Nordy**, LoRa 6U CubeSat, was on target for test. Because its LoRa payload frequency is 436.607kHz which is compatible with THAIIOT LoRa payload. 
For more information about Norby, see https://tinygs.com/satellite/Norbi

![](TinyGS/images/TinyGS_Norby_Information.png)


### TinyGS - THAIIOT (NKRAFA_GCS) Test System Diagram

![](TinyGS/images/TinyGS_ThaiIOT_Test.png)


## How to check result
As test system diagram, once THAIIOT receive packet from Norby and send message to the TinyGS MQTT server. THAIIOT that play as TinyGS ground station role will be shown up on the TinyGS ground station network map with named NKRAFA_GCS

![](TinyGS/images/TinyGS_with_NKRAFA_GCS_Location.png)


Here is the console that show the list of satellites which NKRAFA_GCS received their packets. This console can be monitored in real time at https://tinygs.com/station/NKRAFA_GCS@1728862493

![](TinyGS/images/Tinygs_Station_NKRAFA_GCS_2021_10_27_16_15_17.png)

